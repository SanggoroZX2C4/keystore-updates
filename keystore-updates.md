---
title: OpenPGP Keystore Update Manifest Publication
docname: draft-hagrid-openpgp-keystore-updates-00
date: 2019-11-11
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
 -
    ins: V. Breitmoser
    name: Vincent Breitmoser
    email: look@my.amazin.horse
informative:
 RFC4366:
 RFC5322:
 RFC6960:
 RFC6962:
 RFC7929:
 I-D.shaw-openpgp-hkp:
 I-D.koch-openpgp-webkey-service:
 I-D.mccain-keylist:
normative:
 RFC2047:
 RFC2119:
 RFC4880:
 RFC8174:
 I-D.ietf-openpgp-rfc4880bis:
 I-D.dkg-openpgp-abuse-resistant-keystore:
--- abstract

Many OpenPGP implementations refresh their certificates by retreiving new copies from one or more OpenPGP keystores.
A keystore client that knows which certificates might need to be refreshed from a given keystore can increase the client's efficiency, privacy, and security.
This draft describes a simple mechanism for an OpenPGP keystore to publish an "Update Manifest" -- a list of recently-updated certificates so that a client can optimize its certificate refresh strategy.

--- middle

# Introduction

An OpenPGP client knows of a number of certificates that it cares about.
The composable nature of an OpenPGP certificate means that details of the OpenPGP certificate can change over time, for example by addition or revocation of subkeys or user IDs, updates to cryptographic algorithm preferences, or even revocation of the entire certificate.

An OpenPGP client may refresh any certificate it cares about by re-fetching it from an OpenPGP keystore and merging the returned certificate.

If the keystore hasn't updated the certificate since the client checked last, the entire transaction is wasted, and the client has also informed the keystore that it was interested in that specific certificate.

This draft describes a simple mechanism for an OpenPGP keystore to publish a list of recently-updated certificates.
It also offers implementation guidance for keystores and their clients in manipulating this information to increase efficiency, privacy, and security for all parties involved in the certificate refresh process.

## Requirements Language

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all capitals, as shown here.

## Terminology

This document makes use of the terminology section from {{I-D.dkg-openpgp-abuse-resistant-keystore}}.

# Problem Statement

OpenPGP certificates are composable -- they can shift over time as
packets are added, revoked, expiration dates updated, and possibly
revoked in whole or in part.  These updates are generally infrequent,
but important when they happen, and can be time-sensitive.  An
implementation that knows about one or more cryptographic certificates
generally wants to know whether any certificate it holds has been
updated.

A common practice is to walk through the list of known certificates,
refreshing each one from a public keystore that may have updates
available.  This practice is problematic for several reasons:

## Privacy Concerns (fingerprinting and contact chaining)

A client that asks for all of its certificates at once from the server
leaks the set of entities it is interested in.  This offers a
fingerprinting vector: different users are likely interested in a
different set of certificates.  Furthermore, it supplies an observer
of keystore traffic with information about who is interested in a
specific certificate (who considers them a potential contact), even if
the certificate has not been updated.

## Efficiency Concerns

Consider a client with 200 certificates, and each certificate is
updated on average every 100 days, and the client wants to see an
update no later than 5 days after it has been published to the
keystore.  The client must effectively fetch all certificates every 5
days -- refreshing 20 times the number of certificates that it would
need to fetch under an regime where only the updated certificates are
actually retreived.

## Security Concerns

Given the expense of the network traffic and the privacy concerns,
some clients do not regularly refresh their certificates.  This can
leave the client vulnerable to use or acceptance of a certificate that
has already been revoked.

Furthermore, if the user composes an encrypted message to an
out-of-date certificate, there is an increased chance that the
recipient cannot read the message, incurring an additional round-trip
as the recipient asks the sender to adjust and re-send.  Every
accidentally unreadable message normalizes the workflow of encouraging
the message to be re-sent in the clear, or of disabling encrypted
messaging entirely.


# Keystore Updates Notification Protocol

A keystore can publish a fuzzy list of all certificates that have been
updated in the last short period of time.  Clients can regularly fetch
this fuzzy list, compare it against the certificates they care about,
and refresh the certificates which have a fuzzy match.

By grouping all fetches together with coarse granularity, and by only
supplying a fuzzy match, this scheme reduces the amount of metadata
available to a tracker.

## Temporal Granularity (Epochs)

For the purposes of this update mechanism, the updates cycle in blocks
of 2^15 seconds, or about 9 hours.  They are based on boundaries
associated with Unix timestamps, the same time reference used in
section 3.5 {{RFC4880}}.

Epoch 0 is the 2^15 seconds starting at (and including)
1970-01-01T00:00:00Z, which is Unix timestamp 0.

Epoch 1 is the 2^15 seconds start at (and including)
1970-01-01T09:06:08Z, which is Unix timestamp 32768 (i.e., 2^15).

An epoch is said to be "complete" when all of its seconds are in the
past.  The epoch containing the current time is the "active" epoch.

### Calculating the current epoch

For example, the active epoch (which is by definition not yet
complete) can be calculated in a POSIX-compliant shell with:

    $ echo $(( $(date +%s) / (1<<15) ))

## Update Manifest from Epoch E

An "Update Manifest" from epoch E is a fuzzy listing of all the
certificates whose updates were observed in all complete epochs since
(and including) epoch E.

For a keystore that operates at https://keys.example.net/path/, it can
offer the Update Manifest from epoch E at
https://keys.example.net/path/updates/E, where N is a decimal integer
that identifies an epoch.

The resource SHOULD report a Content-Type of
`application/pgp-keystore-update-manifest`.


As an example, if the current epoch is epoch 47932, then the
most-recent complete epoch is 47931.  Thus the resource at
https://keys.example.net/path/updates/47928 at this time should
contain the set of updates observed by the keystore during epochs
47928, 47929, 47930, and 47931.

Note that it is possible that the retreived resource may include more
epochs than those requested, due to aggregation of epochs in the
distant past.

## Update Manifest "Magic" String

Every Update Manifest MUST begin with a 8-octet "magic" bytestring
E42BAFBDD575770A.

This magic string gives the client some level of assurance that the
retrieved resource is an Update Manifest as described by this
document.

## Fingerprint Prefix "Buckets"

To avoid enumerability, and to offer a more efficient distribution
format, the Update Manifest places each certificate into a "bucket"
based on the first 32 bits of the fingerprint of its primary key.

This fingerprint prefix is independent of the type of fingerprint.
For example, a v4 certificate with a primary key fingerprint of
0xB7BF8FBFBCA4C0CAA558EAE0B266CEC7A307B36C would be in the same bucket
as a v5 certificate with a primary key fingerprint of
0xB7BF8FBFC2A09B0D55835E038B6703EAD2156120AA1C2E60D7C3761ED5A2EA93.
Both would fall into the 0xB7BF8FBF bucket.

This is deliberately *not* the same as the v4 "Key ID", which draws
from the suffix of the fingerprint rather than the prefix.

## Update Manifest Contents

An Update Manifest contains four pieces of information:

 * The "magic" string,

 * The `start_epoch` -- the first epoch included in the manifest,
   encoded as an unsigned 4-octet big-endian integer,

 * The `end_epoch` -- the last epoch included in the manifest, encoded
   as an unsigned 4-octet big-endian integer,

 * A list of fingerprint prefixes whose updates were observed by the
   keystore during the covered range.  Each prefix is the first 32
   bits of a primary key fingerprint.
   
Note that in the case of the most recent Update Manifest,
`start_epoch` will be equal to `end_epoch`.

### Update Manifest Structure

The chart below represents the octets of an update manifest.

~~~
0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|         Magic String          |  start_epoch  |   end_epoch   |
+---------------+---------------+---------------+---------------+
| fpr_prefix[0] | fpr_prefix[1] | fpr_prefix[2] | ...
+---------------+---------------+---------------+
~~~

## Requests for the Distant Past

There will inevitably be past epochs for which a keyserver has no
history of changes (e.g., the period of time before a keystore
existed, or far enough in the past that the keystore had discarded its
records of updates).  The keystore MUST return an HTTP 404 (File not
Found) response code when any of those epochs are requested.

A client that receives an HTTP 404 response from a keystore SHOULD
mark all of its certificates as in need of refresh from that keystore.

## Requests for the Future

Due to clock skew, keystore processing delay in generating the Update
Manifest, or other forms of technical failure, some clients may
request an epoch that the keystore sees as in the future.

The keystore SHOULD return an HTTP 400 (Bad Request) response code
when it sees a request for an epoch in the far future.

A client that receives an HTTP 400 response from a keystore should
verify that its clock is roughly accurate (e.g. via NTP or other time
synchronization protocols); if its clock is rolled back as a result of
this check it should consider re-querying the keystore.  Such a client
MAY also choose to simply decrement the epoch it requests and try
again.

As a convenience, a keystore MAY choose to return the Update Manifest
corresponding to the latest epoch (where `start_epoch` == `end_epoch`)
in response to queries for the currently-active epoch, or other epochs
in the near future.  It SHOULD NOT do this into the 

A client that queries for epoch `E` but receives a response where
`end_epoch` < `E` can infer that either its clock is likely ahead of
the server's clock.

# Implementation Guidance for Clients

## A Robust Certificate Refresh Algorithm

What follows is a robust algorithm for keeping a local cache of
certificates `Certs` up-to-date with respect to a set of keystores.

It depends on fetching an update manifest from a keystore K from epoch
E, described in this notation:

    get_update_manifest(K,E) -> [Start,End,Prefixes]

And fetching the latest copy of certificate `C` from keystore `K` by
fingerprint looks like this:

    C' = certificate_refresh(K,fpr(C)

A client that wants to consider refreshing its entire keyring from a
keystore periodically over time SHOULD record at least 1 bit of
per-keystore state per certificate that it knows about: a
`needs_refresh` flag.  We represent that as `needs_refresh[C,K]`,
where `True` means "the client should refresh the certificate C from
keystore K, as an update may be available". `False` means there is no
need to fetch an update.

The client SHOULD also retain a record of the most recent completed
epoch it is aware of for the keystore.  We represent that as
`latest_epoch[K]`.

We start by initializing the stored data when the client decides to
start working with the keystore.

~~~
Adopt_Keystore(keystore K):
  [_,E,_] = get_update_manifest(K,active_epoch-1)
  for C in Certs:
    needs_refresh[C,K] = True
  latest_epoch[K] = E
~~~

The client should with some frequency ask the keystore for updates
about the certificates it knows about that might need a refresh.  Note
that a privacy-sensitive client MAY choose to randomise the order of
these refreshes, delay them, or perform them through
anonymity-preserving channels like Tor.

~~~
Refresh_Certs(keystore K):
  for C in Certs:
    if needs_refresh[C,K]:
      Certs[C] = OpenPGP_merge(C,keyserver_refresh(K,fpr(C)))
      needs_refresh[C,K] = False
~~~

When the client becomes aware of a new epoch (which might be many
epochs later, for example, if the client was suspended for multiple
days):

~~~
interesting_epoch(keystore K): -> epoch
  return latest_epoch[K]+1

Query_Keystore(keystore K):
 N = interesting_epoch(K)
 [S,E,Prefixes] = get_update_manifest(K,N)
 if E > latest_epoch[K]:
   for P in Prefixes:
     for C in Certs:
       if P == prefix(fpr(C)):
         needs_refresh[C,K] = True
   latest_epoch[K] = E
~~~

Clients SHOULD NOT query for updates from any given keystore more
frequently than once per epoch.  in particular, the client SHOULD NOT
invoke `Query_Keystore` if:

    latest_epoch[K] + 1 >= current_epoch()
    

## Coarser Granularity in the Distant Past

Clients group their queries together by epoch leaking identifying
information about their clocks or their update frequency.  However, a
client that queries for an Update Manifest from an epoch in the
distant past may stand out as unusual.  Accordingly, the farther in
the past that the client queries, the coarser the granularity of
chunks should be, to join a larger anonymity set of clients.

Furthermore, some keystores may aggregate older Update Manifests into
larger chunks for performance or storage reasons, as noted in
{{aggregated-historic-updates}}.  A privacy-preserving client will
want to minimize the per-device leakage while retrieving the same
information.

Accordingly, a privacy-preserving client SHOULD replace the simplistic
`interesting_epoch(K)` (above) with the following method, which aligns
granularities on boundaries with granularity of roughly 3 days, 3
weeks, or 6 months.  `//` represents integer division (discarding the
modulus), and `^` represents exponentiation:

```
interesting_epoch(keystore K): -> epoch
  N = current_epoch()
  for G in 9,6,3:
     if N - latest_epoch[K]+1 > 2^G:
       return ((latest_epoch[K]+1)//2^G)*2^G
  return latest_epoch[K]+1
```


# Implementation Guidance for Keystores

A keystore that receives and merges updates to OpenPGP certificates,
and offers a certificate refresh interface to OpenPGP clients SHOULD
generate and provide Update Manifests to improve client efficiency,
security, and privacy.

## Update Every Epoch, with no gaps

The keystore SHOULD re-populate every Update Manifest it offers at
every epoch.  For example, if a keystore is currently offering Update
Manifests from epochs 100, 101, and 102, ... through 119, and it just
completes epoch 120, it updates all 20 files.

If the updates are served from local files, they should be generated
and atomically moved into place so that no client ever retrieves a
partial Update Manifest.

## List Each Prefix Only Once

Each manifest SHOULD only include a given prefix once.

FIXME: what should a consumer do if it encounters a prefix more than once?

## Sorted Prefixes

The prefixes in each manifest SHOULD be listed in increasing order,
starting at `00000000` and going to `FFFFFFFF`.

Ordered prefixes make it possible for an implementation interested in
a single prefix to do a rapid binary search through a large manifest.

FIXME: what should a consumer do if it encounters out-of-order
prefixes?

## Dropping Ancient History

- FIXME: sliding the update window forward (when do we delete old
  manifests entirely?) -- perhaps based on size?

## Aggregated Historic Updates {#aggregated-historic-updates}

- update aggregation

## Disaster Recovery

- must retain continuous history of updates -- no gaps!

- recovery from failure: just return 404s for the past, and start
  tracking from "now"

# Alternative Implementations

There are other ways to approach this type of work, including bloom
filters, append-only ledgers, key-transparency, etc. These more
sophisticated approaches have additional cryptographic qualities, but
they are also more complex and have a higher barrier to multiple
implementations.

The specification documented here prioritizes simplicity of
implementation over additional features, while providing substantial
improvements over the status quo for certificate refresh.

# Security Considerations

A keystore can attack the client trying to do certificate refresh.  It
can also attack the holder of a certificate that expects the keystore
to redistribute it.  This draft offers some mitigation against both
attacks.

## Client Vulnerability to Keystore Attack

A client that relies on a keystore to learn security-sensitive
information like revocations, expirations, and subkey rotations is
vulnerable to specific kinds of attack by the keystore.

The design of the Update Manifest specified in this document is
intended to make it more difficult for a keystore operator to
distinguish between clients that use the update mechanism, and thus
more difficult to mount an attack targeted at a specific client.

### Attack by Flooding

A keystore is typically hosted on reliable infrastructure with decent
network connectivity.  Clients are often under-resourced by
comparison.  A keystore can attack the client by sending it large
amounts of data, which the keystore has to process.

The Update Manifest mechanism offers a new channel that can be used to
flood the client.  A bandwidth-sensitive client MAY defend against
Update Manifest flooding by declining to fetch any Update Manifest
larger than a pre-determined bound, and simply treating such a
resource as equivalent to a 404 response.

### Attack by Withholding

A keystore can attack a client by choosing to deprive it of updates to
certificates.  The Update Manifest mechanism can also be used to
attack a client by discouraging the client from fetching updates by
claiming that no updates exist.  A client that suspects such an attack
is in progress based on the Update Manifest mechanism MAY choose to
refresh certificates anyway.  This will not protect the from a
withholding attack applied during the certificate refresh operation,
however.

A client concerned about such a targeted attack SHOULD avoid leaking
its identity to the keyserver (see {{privacy-considerations}} for more
details).

## Certificate Holder Vulnerability to Keystore Attack

A keystore may attack the holder of a particular certificate by
universally refusing to update it when it should.  This attack is
detectable by anyone who has a copy of the updated certificate in
question and a statement of the keystore's update policy.

This document introduces a new potential attack, which is a keystore
that accepts an update to a certificate but refuses to list its
fingerprint prefix in the Update Manifest.  However, this attack is
also detectable.  A certificate holder that audits keystore behavior
to detect failure to distribute updates SHOULD also audit the Update
Manifests published by the keystore.

## Attacks on Fingerprint Prefixes

The choice of 32-bit prefixes of fingerprint allows the server to
group certificates into buckets, but does not prevent anyone with
moderate resources from generating a public key with a prefix that
matches any other certificate.

As a result, an attacker who can submit a certificate to a keystore
and update it regularly can cause the keystore to announce an update
in the same "bucket" occupied by a target certificate.

If another client knows about the target certificate and follows the
guidance in this document to do regular certificate refresh based on
the Update Manifest, the attacker can cause that client to refresh the
target certificate once per epoch.

If certificates are encrypted and padded during transport (see
{{padding}}), this is not a particularly strong attack.

## Attacks on the Update Manifest Itself

An attacker who wants to break this update mechanism can generate an
arbitrarily large set of certificates and feed updates to them into
the keystore, causing the Update Manifest to grow up to its maximum
size.

This attack can effectively disable the Update Manifest, forcing
clients back into an "update every certificate all the time" posture.
However, it must be an ongoing and sustained attack to keep the Update
Manifest disabled.  As soon as the attack stops, the Update Manifest
mechanism will work as intended.

# Privacy Considerations {#privacy-considerations}

When a client retrieves a set of certificates from a server, the
combination of certificates requested is likely to uniquely identify
the client to the keystore.  A client, once uniquely identified, can
be subject to various forms of targeted attack by the keystore.
Sensitive clients can mitigate this risk by fetching certificates
individually, in staggered, delayed, and randomized sequences, over
anonymity-preserving channels (e.g. by using Tor).

To avoid linkage between these requests, a keystore client SHOULD also
avoid accepting any of the persistent state mechanisms offered by
HTTPS transit.  In particular, it should avoid at least:

 * sending e-tags
 * sending cookies
 * reusing a TLS session

## Padding Certificates in Transit {#padding}

A certificate distributed over an encrypted channel still leaks its
size to a network observer.  Keystores should pad certificates
distributed over an encrypted channel to avoid size-based traffic
analysis.  For certificates distributed via HTTPS, it's possible to
pad them extraneous HTTP headers.  Alternately, since TLS provides a
padding mechanism, it's possible to pad them at the TLS layer.  This
draft does not contemplate padding mechanisms within the OpenPGP
packet stream itself.

Padding policy (how much to pad) is out of scope for this document.

## Keystore Enumeration

Some keystores implementations intend to discourage clients from
enumerating the full list of certificates that they offer.  The Update
Manifest's listing of fingerprint prefixes is deliberately vague
enough that it should not facilitate any practical enumeration attack.

Keystores that offer an Update Manifest and want to avoid certificate
enumeration MUST NOT offer certificate retrieval by fingerprint
prefix.


# IANA Considerations

Mime-type registration:

    `application/pgp-keystore-update-manifest`
    
Should reference this document.


# Document Considerations

\[ RFC Editor: please remove this section before publication \]

This document is currently edited as markdown.  Minor editorial
changes can be suggested via merge requests at
https://gitlab.com/hagrid-keyserver/keystore-updates or by
e-mail to the author.  Please direct all significant commentary to the
public IETF OpenPGP mailing list: openpgp@ietf.org

## Document History

# Acknowledgements

This document is the result of joint discussion with several other
implementers who have not yet agreed to be listed in this section.

--- back

# Example Update Manifest {#example}

Below is a hexdump of an update manifest that covers epochs from 15296
to 15322 (inclusive).  This corresponds to the time range from
1985-11-18T22:35:28 through 1985-11-29T04:21:03 (inclusive).  The
observed updated certificates had five primary key fingerprint
prefixes:

 * 32144D9D, 65FB1218, 7E91F402, 9ED85A5E, EA71546A
 
~~~
00000000  e4 2b af bd d5 75 77 0a  00 00 3b c0 00 00 3b c0
00000010  32 14 4d 9d 65 fb 12 18  7e 91 f4 02 9e d8 5a 5e
00000020  ea 71 54 6a
~~~

If the keystore client knows of an OpenPGP v4 certificate with a
primary key fingerprint of 0x9ED85A5EB8E354200B0598DFC71BAE8DCBC0738B,
and the client last refreshed this certificate from the keystore
during epoch 15296, then they should mark that certificate as in need
of a refresh from this keystore.

