# OpenPGP Keystore Update Manifest Publication

This repository is home to an Internet Draft that describes how an OpenPGP can facilitate efficient certificate refresh while still preserving some level of client privacy.

The document will eventually be published at https://datatracker.ietf.org/doc/draft-hagrid-openpgp-keystore-updates/
